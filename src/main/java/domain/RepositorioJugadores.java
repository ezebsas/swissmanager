package domain;

import java.util.List;

import org.uqbarproject.jpa.java8.extras.WithGlobalEntityManager;

public class RepositorioJugadores implements WithGlobalEntityManager {

  public static RepositorioJugadores instancia = new RepositorioJugadores();

  public List<Jugador> listar() {
    return entityManager()//
        .createQuery("from Jugador", Jugador.class) //
        .getResultList();
  }

  public Jugador buscar(long id) {
    return entityManager().find(Jugador.class, id);
  }

  public void agregar(Jugador jugador) {
    entityManager().persist(jugador);
  }

  public List<Jugador> buscarPorNombre(String nombre) {
    return entityManager() //
        .createQuery("from Jugador j where j.nombre like :nombre", Jugador.class) //
        .setParameter("nombre", "%" + nombre + "%") //
        .getResultList();
  }

}
