package domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Jugadores")
public class Jugador {
	
	public Jugador(String string, int i) {
		nombre = string;
		elo = i;
	}
	
	public Jugador() {
		
	}
	@Id
	@GeneratedValue
	private long id;

	private String nombre;
	private Integer elo;
	private Sexo sexo;
	private LocalDate fechaNacimiento;
	private String titulo;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getElo() {
		return elo;
	}

	public void setElo(Integer elo) {
		this.elo = elo;
	}
	
}
