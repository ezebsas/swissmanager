package controllers;

import java.util.HashMap;
import java.util.List;

import domain.Jugador;
import domain.RepositorioJugadores;

import org.uqbarproject.jpa.java8.extras.WithGlobalEntityManager;
import org.uqbarproject.jpa.java8.extras.transaction.TransactionalOps;

import spark.ModelAndView;
import spark.Request;
import spark.Response;

public class JugadoresControllers implements WithGlobalEntityManager, TransactionalOps {
	
	public ModelAndView nuevo(Request request, Response response) {
	    return new ModelAndView(null, "jugador-nuevo.hbs");
	  }
	
	public ModelAndView listar(Request request, Response response) {
		 List<Jugador> jugadores;	         	 
		 jugadores = RepositorioJugadores.instancia.listar();
		 
		 HashMap<String, Object> viewModel = new HashMap<>();
		 viewModel.put("jugadores", jugadores);

		 return new ModelAndView(viewModel, "jugadores.hbs");
	  }
	
	public Void crear(Request request, Response response) {
	    String nombre = request.queryParams("nombre");
	    int elo = Integer.parseInt(request.queryParams("elo"));

	    withTransaction(() -> {
	      RepositorioJugadores.instancia.agregar(new Jugador(nombre, elo));
	    });

	    response.redirect("/jugadores");
	    return null;
	  }

}
