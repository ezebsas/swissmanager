package controllers;

import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;

public class HomeController {

  
  public ModelAndView principal(Request request, Response response) {
	  return new ModelAndView(null, "home.hbs");
  }
  

}
