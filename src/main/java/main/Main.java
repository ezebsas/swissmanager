package main;

import java.util.HashMap;
import java.util.List;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.SparkBase.port;
import static spark.SparkBase.staticFileLocation;
import spark.ModelAndView;
import spark.template.handlebars.HandlebarsTemplateEngine;

import controllers.HomeController;
import controllers.JugadoresControllers;

public class Main {

  public static void main(String[] args) {
    System.out.println("Iniciando servidor");
    
    HomeController home = new HomeController();
    JugadoresControllers jugadores = new JugadoresControllers();
    HandlebarsTemplateEngine engine = new HandlebarsTemplateEngine();

    port(8080);
    staticFileLocation("/public");

    get("/", home::principal, engine);
    get("/index.html", (request, response) -> {
        response.redirect("/");
        return null;
      });
    get("/jugadores", jugadores::listar, engine);
    post("/jugadores", jugadores::crear);
    get("/jugadores/new", jugadores::nuevo, engine);
    
  }

}